﻿using BridgeMonitor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BridgeMonitor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var dates = getDateListFromApi();
            dates = dates.OrderBy(Date => Date.ClosingDate).ToList();
            return View(dates);
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Toutes_Fermetures()
        {
            var dates = getDateListFromApi();
            dates = dates.OrderBy(Date => Date.ClosingDate).ToList();
            return View(dates);
        }

        public IActionResult Details(string date)
        {
            var closing = getDateListFromApi();
            List<Date> details = new List<Date>();
            foreach (var closing_date in closing)
            {
                if (closing_date.ClosingDate.ToString("yyyyMMddHHmmss") == date)
                {
                    details.Add(closing_date);
                }
            }
            return View(details);
        }

        private static readonly HttpClient client = new HttpClient();
        public static List<Date> getDateListFromApi()
        {
            var url = client.GetStringAsync("https://api.alexandredubois.com/pont-chaban/api.php");
            var donnee_json = url.Result;
            var resultat = JsonConvert.DeserializeObject<List<Date>>(donnee_json);
            return resultat;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
    }
}
